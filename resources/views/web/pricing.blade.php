@extends('web.layouts.web')
@section('title','pricing')
@section('main')

    <main>
        <div class="innerPages py-15 py-lg-25" style="background-image:url({{asset('web/images/oz-about.png')}})">
            <h2 class="text-capitalize text-center text-white">Contact us</h2>
        </div>
        <div class="pricingSec mb-lg-4 pt-7 pt-lg-10">
            <div class="container">
                <h2 class="text-center mb-10">Price</h2>
                <div class="row">
                    <div class="col-md-6 col-lg-4">
                        <div class="pricingBlog text-center">
                            <span class="d-inline-block type text-capitalize">Free</span>
                            <div class="priceHead text-center">
                                <strong class="price"><sup>$</sup>0.0</strong>
                                <span class="off">&nbsp;</span>
                            </div>
                            <div class="priceBody text-left">
                                <ul>
                                    <li><span class="icon-tick"></span>At one time, you can create one temporary email.</li>
                                    <li><span class="icon-tick"></span>Up to 3 hours, email remains safe.</li>
                                    <li><span class="icon-tick"></span>Advertisement</li>
                                </ul>
                                <a href="#" class="btn">Buy Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="pricingBlog priceStandard text-center">
                            <span class="d-inline-block type text-capitalize">Standard</span>
                            <div class="priceHead text-center">
                                <strong class="price"><sup>$</sup>10</strong>
                                <span class="off">&nbsp;</span>
                            </div>
                            <div class="priceBody text-left">
                                <ul>
                                    <li><span class="icon-tick"></span>At one time, you can create one temporary email.</li>
                                    <li><span class="icon-tick"></span>Up to 3 hours, email remains safe.</li>
                                    <li><span class="icon-tick"></span>Advertisement</li>
                                </ul>
                                <a href="#" class="btn mx-auto">Buy Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="pricingBlog pricePremium text-center">
                            <span class="d-inline-block type text-capitalize">Premium</span>
                            <div class="priceHead text-center">
                                <strong class="price"><sup>$</sup>60</strong>
                                <span class="off">(50% OFF)</span>
                            </div>
                            <div class="priceBody text-left">
                                <ul>
                                    <li><span class="icon-tick"></span>At one time, you can create one temporary email.</li>
                                    <li><span class="icon-tick"></span>Up to 3 hours, email remains safe.</li>
                                    <li><span class="icon-tick"></span>Advertisement</li>
                                </ul>
                                <a href="#" class="btn mx-auto">Buy Now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
