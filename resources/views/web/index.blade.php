@extends('web.layouts.web')
@section('title','index')
@section('style')
    <link rel="stylesheet" href="{{asset('web/css/owl.carousel.min.css')}}">
    <script type="text/javascript">
        window.addEventListener("resize", function () {
            "use strict";
            window.location.reload();
        });
        document.addEventListener("DOMContentLoaded", function () {
            // make it as accordion for smaller screens
            if (window.innerWidth > 992) {
                document.querySelectorAll('.navbar .nav-item').forEach(function (everyitem) {
                    everyitem.addEventListener('mouseover', function (e) {
                        let el_link = this.querySelector('a[data-bs-toggle]');
                        if (el_link != null) {
                            let nextEl = el_link.nextElementSibling;
                            el_link.classList.add('show');
                            nextEl.classList.add('show');
                        }
                    });
                    everyitem.addEventListener('mouseleave', function (e) {
                        let el_link = this.querySelector('a[data-bs-toggle]');
                        if (el_link != null) {
                            let nextEl = el_link.nextElementSibling;
                            el_link.classList.remove('show');
                            nextEl.classList.remove('show');
                        }
                    })
                });
            }
            // end if innerWidth
        });
        // DOMContentLoaded  end
    </script>
@endsection
@section('main')
    <main>
        <div class="pageBanner">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <div class="bannerCaption text-white">
                            <span class="bannerTitle d-block mb-1">Web Design Company</span>
                            <h1 class="text-white">Website Design and Development Company</h1>
                            <p>StartDesigns gives inventive web design services that convey your business more
                                effectively
                                online to visitors for the first time. We have an in-house team of web designers with
                                10+ years
                                of experience with a 95% success rate in all industries. Start Designs giving creative
                                and
                                responsive website design solutions that enhance the user experience from every platform
                                to
                                convert your potential customers in your client.</p>
                            <div class="btnHolder d-flex pt-2 pt-lg-3">
                                <a href="{{route('contact-us-form')}}" class="btn btn-red mr-4">contact support <span
                                        class="icon-arrow arrow-btn"></span></a>
                                <a href="file:///D:/oz-web-development/markup/contact-us-2.html" class="btn btn-yellow">start
                                    project <span class="icon-arrow arrow-btn"></span></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="imgHolder">
                            <img src="{{asset('web/images/webb-designn-bennr.png')}}" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="formSec py-7 py-lg-10" id="form-project">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-lg-7">
                        <div class="textHolder pt-4">
                            <h2 class="mb-4 text-capitalize">Form</h2>
                            <p>Hire StartDesigns on hourly, weekly or full time basis to design your website. A website
                                can be
                                your dream, business or a form of marketing. We at StartDesigns have the best website
                                designers
                                who can help you create a target oriented, adaptive, appealing, user friendly, efficient
                                website. We offer beautiful elegant templates, customizable layouts, web integrations,
                                mobile
                                optimized design and a lot more. Whatever you desire we create using wordpress, magento,
                                ruby on
                                rails, etc.</p>
                            <ul class="list-unstyled formList text-capitalize mb-0">
                                <li><span class="icon-arrow arrow-btn"></span> list 1</li>
                                <li><span class="icon-arrow arrow-btn"></span> list 1</li>
                                <li><span class="icon-arrow arrow-btn"></span> list 1</li>
                                <li><span class="icon-arrow arrow-btn"></span> list 1</li>
                                <li><span class="icon-arrow arrow-btn"></span> list 1</li>
                                <li><span class="icon-arrow arrow-btn"></span> list 1</li>
                                <li><span class="icon-arrow arrow-btn"></span> list 1</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-5">
                        <form action="#" class="formBlock">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Name:</label>
                                <input type="email" class="form-control" id="exampleInputEmail1"
                                       aria-describedby="emailHelp"
                                       placeholder="Name">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email address</label>
                                <input type="email" class="form-control" id="exampleInputEmail1"
                                       aria-describedby="emailHelp"
                                       placeholder="Email Address">
                            </div>
                            <div class="form-group mb-5">
                                <label for="exampleInputEmail1">Subject</label>
                                <input type="text" class="form-control" id="exampleInputEmail1"
                                       aria-describedby="emailHelp"
                                       placeholder="Subject">
                            </div>
                            <div class="form-group mb-5">
                                <label for="exampleInputEmail1">Message</label>
                                <textarea class="form-control" placeholder="Subject"></textarea>
                            </div>
                            <button type="submit" class="btn btn-yellow mx-auto">Submit <span
                                    class="icon-arrow arrow-btn"></span></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="featureSec bgGrey  py-7 py-lg-10mb-2 mb-lg-10">
            <div class="container">
                <h2 class="text-capitalize text-center mb-6">Web Design And Development Solutions <span
                        class="headingColor">That We Offer</span>
                </h2>

                <div class="row justify-content-center">
                    <div class="col-md-6 col-lg-4">
                        <a href="#" class="featureCol d-block text-center">
                            <div class="ico">
                                <span class="icon-web-dev"></span>
                            </div>
                            <div class="txt">
                                <h3 class="text-capitalize">web development</h3>
                                <p>Start Designs giving creative and responsive.</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <a href="#" class="featureCol feature-col-2 d-block text-center">
                            <div class="ico">
                                <span class="icon-web-design"></span>
                            </div>
                            <div class="txt">
                                <h3 class="text-capitalize">web designing</h3>
                                <p>Start Designs giving creative and responsive website design solutions that enhance
                                    the user
                                    experience from every platform to convert your potential customers in your
                                    client.</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <a href="#" class="featureCol feature-col-3 d-block text-center">
                            <div class="ico">
                                <span class="icon-mobile-dev"></span>
                            </div>
                            <div class="txt">
                                <h3 class="text-capitalize">app development</h3>
                                <p>Start Designs giving creative and responsive website design solutions that enhance
                                    the user
                                    experience from every platform to convert your potential customers in your
                                    client.</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="awardSec mb-7 mb-lg-10">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h2 class="mb-4 text-capitalize">Awarded <span
                                class="headingColor">Website Design Company</span></h2>
                        <p>We craft what you think, one of our USP is designing web pages to present your business in an
                            enchanting way on the web. We are serving for the last 5 years in the USA, UK, and India
                            with our
                            skilled and highly professional web designers. By innovative design solutions, we increase
                            not only
                            our expertise but also become a family of 150+ delighted customers globally.</p>
                        <p>You can hire our web designers on different hiring modules. Our hiring modules are hourly,
                            weakly,
                            monthly, and project basis. It allows you the freedom to choose according to your
                            requirements and
                            project need. So, choose us as your web solution partner today.</p>
                    </div>
                    <div class="col-md-6">
                        <div class="imgHolder text-right">
                            <img src="{{asset('web/images/web-dev1.png')}}" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ChooseSec mb-7 mb-lg-10 pt-7 pt-lg-10">
            <div class="container">
                <h2 class="text-capitalize text-center mb-6 text-white"><span>Why Choose Our Website Design Services In australia
            ?</span></h2>
                <div class="row justify-content-center">
                    <div class="col-md-6">
                        <div class="chooseCol animationCol" style="background: #fff;">
                            <div class="ico">
                                <img src="{{asset('web/images/team.svg')}}" class="img-fluid">
                            </div>
                            <div class="txt">
                                <h3 class="text-capitalize">Experience Team</h3>
                                <p>Our experienced team is the key to success that handle any project impressively. Our
                                    experience helps us to complete our professional goals in time without any
                                    hurdles.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="chooseCol animationCol" style="background: #fff;">
                            <div class="ico">
                                <img src="{{asset('web/images/on-time.svg')}}" class="img-fluid">
                            </div>
                            <div class="txt">
                                <h3 class="text-capitalize">On-time Delivery</h3>
                                <p>While we having a team of highly professional developers and experts we able to
                                    deliver
                                    projects on time. Without any comprise in the quality of products and services. We
                                    do
                                    bug-free codding.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="chooseCol animationCol" style="background: #fff;">
                            <div class="ico">
                                <img src="{{asset('web/images/creative-idea.svg')}}" class="img-fluid">
                            </div>
                            <div class="txt">
                                <h3 class="text-capitalize">Creative Ideas</h3>
                                <p>Being a leading website design company we have the reputation of designing the most
                                    inspiring
                                    and creative ideas that help customers to build a unique identity through the online
                                    presence in the industry.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="chooseCol animationCol" style="background: #fff;">
                            <div class="ico">
                                <img src="{{asset('web/images/approch.svg')}}" class="img-fluid">
                            </div>
                            <div class="txt">
                                <h3 class="text-capitalize">Professional Approach</h3>
                                <p>Our approach is more professional. We always listen to the requirements and ideas of
                                    our
                                    customers. Share our experience to improve and provides that our customer
                                    thinks.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="chooseCol animationCol" style="background: #fff;">
                            <div class="ico">
                                <img src="{{asset('web/images/nda.svg')}}" class="img-fluid">
                            </div>
                            <div class="txt">
                                <h3 class="text-capitalize">Non Disclosure Agreement (NDA) Policy</h3>
                                <p>We are strictly concern about our client data, we make it confidential to provide
                                    more
                                    securities to our customers. We always follow the NDA policy.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="chooseCol animationCol" style="background: #fff;">
                            <div class="ico">
                                <img src="{{asset('web/images/support.svg')}}" class="img-fluid">
                            </div>
                            <div class="txt">
                                <h3 class="text-capitalize">Customer Support</h3>
                                <p>As an emerging eCommerce web designing company, we ensure to provides 24x7 customer
                                    support.
                                    We help clients to make familiar about using CMS, which we use to build an eCommerce
                                    website.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="portfolioSec mb-15">
            <div class="container">
                <div class="portfolioSlider owl-carousel">
                    <div class="imgHolder text-center">
                        <img src="{{asset('web/images/tempmail.png')}}" class="img-fluid mx-auto">
                    </div>
                    <div class="imgHolder">
                        <img src="{{asset('web/images/tempmail.png')}}" class="img-fluid mx-auto">
                    </div>
                    <div class="imgHolder">
                        <img src="{{asset('web/images/tempmail.png')}}" class="img-fluid mx-auto">
                    </div>
                </div>
            </div>
        </div>
        <div class="aboutSec bgGrey py-7 py-lg-10 mb-7 mb-lg-10">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h2 class="mb-4 text-capitalize">Website <span class="headingColor">Design Company</span></h2>
                        <p>Hire StartDesigns on hourly, weekly or full time basis to design your website. A website can
                            be your
                            dream, business or a form of marketing. We at StartDesigns have the best website designers
                            who can
                            help you create a target oriented, adaptive, appealing, user friendly, efficient website. We
                            offer
                            beautiful elegant templates, customizable layouts, web integrations, mobile optimized design
                            and a
                            lot more. Whatever you desire we create using wordpress, magento, ruby on rails, etc.</p>
                        <a href="{{route('contact-us-form')}}" class="btn btn-red ">contact
                            support
                            <span class="icon-arrow arrow-btn"></span></a>
                    </div>
                    <div class="col-md-6">
                        <div class="imgHolder">
                            <img src="{{asset('web/images/about.png')}}" class="img-fluid animate">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="questionSec mb-7 mb-lg-10">
            <div class="container">
                <h2 class="mb-4 mb-lg-6 text-center">Frequently <span class="headingColor">Asked Questions</span></h2>
                <div class="accordion" id="accordionExample">
                    <div class="card cardBlock">
                        <div class="card-header cardHeader" id="headingOne">
                            <h3 class="mb-0">
                                <button class="btn btn-link accordionOpener" type="button" data-toggle="collapse"
                                        data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                    <strong>What is the nearest Auto shop?</strong>
                                    <i class="icon-plus icn"></i>
                                </button>
                            </h3>
                        </div>
                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <p>I searched on google and found that perthautorepair is the Best Auto repair near
                                    me.</p>
                            </div>
                        </div>
                    </div>
                    <div class="card cardBlock">
                        <div class="card-header cardHeader" id="headingTwo">
                            <h2 class="mb-0">
                                <button class="btn btn-link accordionOpener" type="button" data-toggle="collapse"
                                        data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <strong>What is the nearest Auto shop?</strong>
                                    <i class="icon-plus icn"></i>
                                </button>
                            </h2>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <p>As we are committed to provide quality repair service, it means we will only use
                                    high-standard, quality-tested and original spare parts for your vehicle.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="blogSec mb-2 mb-lg-5">
            <div class="container">
                <h2 class="text-capitalize text-center mb-6">Read The Tech Stories Of <span class="headingColor">The Week</span>
                </h2>
                <div class="row justify-content-center">
                    <div class="col-md-6 col-lg-4">
                        <a href="" class="blogCol d-block">
                            <div class="imgHolder">
                                <img src="{{asset('web/images/blog-3.png')}}" class="img-fluid">
                            </div>
                            <div class="txt">
                                <span class="d-block text-uppercase title">WEB DESIGN, WEB DEVELOPMENT</span>
                                <h3 class="text-capitalize">Top Website Builders for Small Business: 2022</h3>
                                <p>Start Designs giving creative and responsive website design solutions that enhance
                                    the user
                                    experience from every platform to convert your potential customers in your
                                    client.</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <a href="" class="blogCol d-block">
                            <div class="imgHolder">
                                <img src="{{asset('web/images/blog-3.png')}}" class="img-fluid">
                            </div>
                            <div class="txt">
                                <span class="d-block text-uppercase title">WEB DESIGN, WEB DEVELOPMENT</span>
                                <h3 class="text-capitalize">Top Website Builders for Small Business: 2022</h3>
                                <p>Start Designs giving creative and responsive website design solutions that enhance
                                    the user
                                    experience from every platform to convert your potential customers in your
                                    client.</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <a href="" class="blogCol d-block">
                            <div class="imgHolder">
                                <img src="{{asset('web/images/blog-3.png')}}" class="img-fluid">
                            </div>
                            <div class="txt">
                                <span class="d-block text-uppercase title">Blogging, WEB DESIGN</span>
                                <h3 class="text-capitalize">Top Website Builders for Small Business: 2022</h3>
                                <p>Start Designs giving creative and responsive website design solutions that enhance
                                    the user
                                    experience from every platform to convert your potential customers in your
                                    client.</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
@section('js')
    <script src="{{asset('web/js/smooth-scroll.min.js')}}"></script>
    <script src="{{asset('web/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('web/js/custom.js')}}"></script>
@endsection
