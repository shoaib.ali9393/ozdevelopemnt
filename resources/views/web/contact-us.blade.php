@extends('web.layouts.web')
@section('title','contact us')
@section('main')
    <main>
        <div class="innerPages py-15 py-lg-25" style="background-image:url({{asset('web/images/oz-about.png')}})">
            <h2 class="text-capitalize text-center text-white">Contact us</h2>
        </div>
        <div class="contactSec py-7 py-lg-10">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="contactText">
                            <div class="txt mb-3">
                                <h6 class="text-capitalize mb-1">Corporate Office</h6>
                                <span class="d-flex align-items-start mb-4"><span class="icon-location mt-1 mr-1"></span>Davis Road Plaza RB Heights 1st Floor office no 103, Lahore, 54000</span>
                            </div>
                            <div class="txt">
                                <h6 class="text-capitalize mb-1">Development Office</h6>
                                <span class="d-flex align-items-start"><span class="icon-location mt-1 mr-1"></span>Davis Road Plaza RB Heights 1st Floor office no 103, Lahore, 54000</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <ul class="list-unstyled contactList d-flex">
                            <li>
                                <div class="ico">
                                    <img src="{{asset('web/images/icon-contact-2.png')}}" class="img-fluid">
                                </div>
                                <h6 class="text-capitalize mb-1">Phone Number</h6>
                                <a href="#123 4567 345">123 4567 345</a>
                            </li>
                            <li>
                                <div class="ico">
                                    <img src="{{asset('web/images/icon-contact-3.png')}}" class="img-fluid">
                                </div>
                                <h6 class="text-capitalize mb-1">email id</h6>
                                <a href="#123 4567 345">appdevzone@gmail.com</a>
                            </li>
                            <li>
                                <div class="ico">
                                    <img src="{{asset('web/images/icon-contact-4.png')}}" class="img-fluid">
                                </div>
                                <h6 class="text-capitalize mb-1">work hours</h6>
                                <span class="d-block">Monday to Friday </span>
                                <span>9:30AM to 6:30PM(GMT)</span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="contactBlock mt-7 mt-lg-10">
                    <div class="row no-gutters">
                        <div class="col-md-6">
                            <div class="py-4 px-3">
                                <h2 class="text-capitalize mb-6 text-center text-white">contact us</h2>
                                <form action="#" class="contactForm">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Name:</label>
                                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email address</label>
                                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email Address">
                                    </div>
                                    <div class="form-group mb-5">
                                        <label for="exampleInputEmail1">Subject</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Subject">
                                    </div>
                                    <div class="form-group mb-5">
                                        <label for="exampleInputEmail1">Message</label>
                                        <textarea class="form-control" placeholder="Subject"></textarea>
                                    </div>
                                    <button type="submit" class="btn btn-yellow mx-auto">Submit <span class="icon-arrow arrow-btn"></span></button>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="map h-100">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13599.065346842892!2d74.30469834103494!3d31.558026671587882!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x391904abc424fe89%3A0x75fc7243a61d72f9!2sMozang%20Chungi%2C%20Lahore%2C%20Punjab%2054000%2C%20Pakistan!5e0!3m2!1sen!2s!4v1670827264687!5m2!1sen!2s" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
