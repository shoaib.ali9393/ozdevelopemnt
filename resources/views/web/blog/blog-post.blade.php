@extends('web.layouts.web')
@section('title','Blog-post')
@section('main')
    <main>
        <div class="innerPages py-15 py-lg-25" style="background-image:url({{asset('web/images/oz-about.png')}})">
            <h2 class="text-capitalize text-center text-white">Blog Post</h2>
        </div>
        <div class="blogPost mb-7 mb-lg-10">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-12 col-lg-11">
                        <div class="post">
                            <h1 class="text-capitalize text-black">Top Website Builders for Small Business: 2022</h1>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                            <div class="imgHolder">
                                <img src="{{asset('web/images/blog-post.png')}}" class="img-fluid" style="height: 210px;width: 100%;">
                            </div>
                            <div class="txt">
                                <span class="d-block text-capitalize date mb-2">December 12 2022</span>
                                <h2 class="text-capitalize text-black">Top Website Builders for Small Business: 2022</h2>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                <h3 class="text-capitalize text-black">Top Website Builders for Small Business: 2022</h3>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the  including versions of Lorem Ipsum.</p>
                                <h4 class="text-capitalize text-black">Top Website Builders for Small Business: 2022</h4>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into including versions of Lorem Ipsum.</p>
                                <h5 class="text-capitalize text-black">Top Website Builders for Small Business: 2022</h5>
                                <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                <h6 class="text-capitalize text-black">Top Website Builders for Small Business: 2022</h6>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
