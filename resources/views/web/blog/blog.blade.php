@extends('web.layouts.web')
@section('title','Blog')
@section('main')
    <main>
        <div class="innerPages py-20 mb-10" style="background-image:url({{asset('web/images/oz-about.png')}})">
            <h2 class="text-capitalize text-center text-white">Blog</h2>
        </div>
        <div class="blogSec mb-2 mb-lg-5">
            <div class="container">
                <h2 class="text-capitalize text-center mb-6">Read The Tech Stories Of <span class="headingColor">The Week</span></h2>
                <div class="row justify-content-center">
                    <div class="col-md-6 col-lg-4">
                        <a href="" class="blogCol d-block">
                            <div class="imgHolder">
                                <img src="{{asset('web/images/blog-3.png')}}" class="img-fluid">
                            </div>
                            <div class="txt">
                                <span class="d-block text-uppercase title">WEB DESIGN, WEB DEVELOPMENT</span>
                                <h3 class="text-capitalize">Top Website Builders for Small Business: 2022</h3>
                                <p>Start Designs giving creative and responsive website design solutions that enhance the user experience from every platform to convert your potential customers in your client.</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <a href="" class="blogCol d-block">
                            <div class="imgHolder">
                                <img src="{{asset('web/images/blog-3.png')}}" class="img-fluid">
                            </div>
                            <div class="txt">
                                <span class="d-block text-uppercase title">WEB DESIGN, WEB DEVELOPMENT</span>
                                <h3 class="text-capitalize">Top Website Builders for Small Business: 2022</h3>
                                <p>Start Designs giving creative and responsive website design solutions that enhance the user experience from every platform to convert your potential customers in your client.</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <a href="" class="blogCol d-block">
                            <div class="imgHolder">
                                <img src="{{asset('web/images/blog-3.png')}}" class="img-fluid">
                            </div>
                            <div class="txt">
                                <span class="d-block text-uppercase title">Blogging, WEB DESIGN</span>
                                <h3 class="text-capitalize">Top Website Builders for Small Business: 2022</h3>
                                <p>Start Designs giving creative and responsive website design solutions that enhance the user experience from every platform to convert your potential customers in your client.</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <a href="" class="blogCol d-block">
                            <div class="imgHolder">
                                <img src="{{asset('web/images/blog-3.png')}}" class="img-fluid" style="height: 200px;width: 100%;">
                            </div>
                            <div class="txt">
                                <span class="d-block text-uppercase title">Blogging, WEB DESIGN</span>
                                <h3 class="text-capitalize">Top Website Builders for Small Business: 2022</h3>
                                <p>Start Designs giving creative and responsive website design solutions that enhance the user experience from every platform to convert your potential customers in your client.</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <a href="" class="blogCol d-block">
                            <div class="imgHolder">
                                <img src="{{asset('web/images/blog-3.png')}}" class="img-fluid" style="height: 200px;width: 100%;">
                            </div>
                            <div class="txt">
                                <span class="d-block text-uppercase title">Blogging, WEB DESIGN</span>
                                <h3 class="text-capitalize">Top Website Builders for Small Business: 2022</h3>
                                <p>Start Designs giving creative and responsive website design solutions that enhance the user experience from every platform to convert your potential customers in your client.</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <a href="" class="blogCol d-block">
                            <div class="imgHolder">
                                <img src="{{asset('web/images/blog-3.png')}}" class="img-fluid" style="height: 200px;width: 100%;">
                            </div>
                            <div class="txt">
                                <span class="d-block text-uppercase title">Blogging, WEB DESIGN</span>
                                <h3 class="text-capitalize">Top Website Builders for Small Business: 2022</h3>
                                <p>Start Designs giving creative and responsive website design solutions that enhance the user experience from every platform to convert your potential customers in your client.</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
