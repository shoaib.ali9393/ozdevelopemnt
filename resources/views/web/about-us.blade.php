@extends('web.layouts.web')
@section('title','about us')
@section('main')
    <main>
        <div class="innerPages py-15 py-lg-25 mb-7 mb-lg-10"
             style="background-image:url({{asset('web/images/oz-about.png')}})">
            <h2 class="text-capitalize text-center text-white">About us</h2>
        </div>
        <div class="about-us mb-10">
            <div class="container">
                <div class="aboutblog">
                    <div class="row">
                        <div class="col-md-6 order-md-2">
                            <h2 class="mb-4 text-capitalize">oz web development <span
                                    class="headingColor">Development</span></h2>
                            <p>Hire StartDesigns on hourly, weekly or full time basis to design your website. A website
                                can be your dream, business or a form of marketing. We at StartDesigns have the best
                                website designers who can help you create a target oriented, adaptive, appealing, user
                                friendly, efficient website. We offer beautiful elegant templates, customizable layouts,
                                web integrations, mobile optimized design and a lot more. Whatever you desire we create
                                using wordpress, magento, ruby on rails, etc.</p>
                        </div>
                        <div class="col-md-6 order-md-1">
                            <div class="imgHolder">
                                <img src="{{asset('web/images/app-about.jpeg')}}" class="img-fluid">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="blogSec mb-2 mb-lg-10">
            <div class="container">
                <h2 class="text-capitalize text-center mb-6">Read The Tech Stories Of <span class="headingColor">The Week</span>
                </h2>
                <div class="row justify-content-center">
                    <div class="col-md-6 col-lg-4">
                        <a href="" class="blogCol d-block">
                            <div class="imgHolder">
                                <img src="{{asset('web/images/blog-3.png')}}" class="img-fluid">
                            </div>
                            <div class="txt">
                                <span class="d-block text-uppercase title">WEB DESIGN, WEB DEVELOPMENT</span>
                                <h3 class="text-capitalize">Top Website Builders for Small Business: 2022</h3>
                                <p>Start Designs giving creative and responsive website design solutions that enhance
                                    the user experience from every platform to convert your potential customers in your
                                    client.</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <a href="" class="blogCol d-block">
                            <div class="imgHolder">
                                <img src="{{asset('web/images/blog-3.png')}}" class="img-fluid">
                            </div>
                            <div class="txt">
                                <span class="d-block text-uppercase title">WEB DESIGN, WEB DEVELOPMENT</span>
                                <h3 class="text-capitalize">Top Website Builders for Small Business: 2022</h3>
                                <p>Start Designs giving creative and responsive website design solutions that enhance
                                    the user experience from every platform to convert your potential customers in your
                                    client.</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <a href="" class="blogCol d-block">
                            <div class="imgHolder">
                                <img src="{{asset('web/images/blog-3.png')}}" class="img-fluid">
                            </div>
                            <div class="txt">
                                <span class="d-block text-uppercase title">Blogging, WEB DESIGN</span>
                                <h3 class="text-capitalize">Top Website Builders for Small Business: 2022</h3>
                                <p>Start Designs giving creative and responsive website design solutions that enhance
                                    the user experience from every platform to convert your potential customers in your
                                    client.</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
