<div class="footerPage" style="background-image:url({{asset('web/images/footer-bg.webp')}});">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4 footerCol">
                <div class="footerLogo mb-4">
                    <a href="#" class="d-inline-block"><img src="{{asset('web/images/logo1.2.png')}}" class="img-fluid"
                                                            alt="Oz-web-development"></a>
                </div>
                <div class="footerText">
                    <p>In publishing and graphic design, Lorem ipsum </p>
                    <ul class="list-unstyled d-flex mb-5 socialIcons">
                        <li>
                            <a href="#" target="_blank"><i class="icon-Facebook"></i></a>
                        </li>
                        <li>
                            <a href="#" target="_blank"><i class="icon-instagram"></i></a>
                        </li>
                        <li>
                            <a href="#" target="_blank"><i class="icon-twitter"></i></a>
                        </li>
                        <li>
                            <a href="#" target="_blank"><i class="icon-linkedin"></i></a>
                        </li>
                    </ul>
                    <ul class="list-unstyled socialIcons-2">
                        <li>
                            <a href="mailto:appdevzone@gmail.com" target="_blank">
                                <i class="icon-email"></i>
                                <span class="d-inline-block">appdevzone@gmail.com</span>
                            </a>
                        </li>
                        <li>
                            <a href="#12345678" target="_blank">
                                <i class="icon-ring-call"></i>
                                <span class="d-inline-block">12345678</span>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
            <div class="col-12 col-md-4 footerCol">
                <div class="footerList">
                    <h5 class="mb-4 text-capitalize">Services:</h5>
                    <ul class="list-unstyled mb-0 list">
                        <li>
                            <a href="#">car air conditioning service</a>
                        </li>
                        <li>
                            <a href="#">car brake services</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-12 col-md-4 footerCol">
                <div class="footerList">
                    <h5 class="mb-4 text-capitalize">Visit Now:</h5>
                    <ul class="list-unstyled mb-0 list">
                        <li>
                            <a href="{{route('about-us')}}">about us</a>
                        </li>
                        <li>
                            <a href="{{route('contact-us')}}">contact us</a>
                        </li>
                        <li>
                            <a href="{{route('pricing')}}">pricing</a>
                        </li>
                        <li>
                            <a href="{{route('pricing')}}">pricing</a>
                        </li>
                        <li>
                            <a href="{{route('blog')}}">Blog</a>
                        </li>
                        <li>
                            <a href="{{route('blog-post')}}">Blog Posts</a>
                        </li>
                        <li>
                            <a href="#">privacy</a>
                        </li>
                        <li>
                            <a href="#">Terms and Conditions</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="copyright text-center py-3">
    <span>Copyright &copy; 2022 <a class="d-inline-block text-capitalize" href="#"> ozWebDevelopment </a></span>
</div>

