    <div class="container">
        <div class="row align-items-center">
            <div class="col-5 col-md-3 col-xl-3">
                <div class="logo">
                    <a href="file:///D:/oz-web-development/markup/index.html"><img src="{{asset('web/images/logo-oz-side.png')}}" class="img-fluid" alt="perthautorepair"></a>
                </div>
            </div>
            <div class="col-7 col-md-9 col-xl-9 position-static d-flex justify-content-end">
                <div class="navAside">
                    <nav id="pageNav" class="navbar navbar-expand-lg position-static">
                        <a href="#" class="navbar-toggler pgNavOpener position-relative" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="icnBar"><span class="srOnly">menu</span></span>
                        </a>
                        <div class="collapse navbar-collapse pageNavCollapse" id="navbarSupportedContent">
                            <ul class="navbar-nav pageNavbar text-capitalize">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('index')}}">home</a>
                                </li>
                                <li class="nav-item dropdown navDropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        our Services
                                    </a>
                                    <div class="dropdown-menu dropdownMenu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{route('web-development')}}">Web Development</a>
                                        <a class="dropdown-item" href="{{route('app-development')}}">App Development</a>
                                        <a class="dropdown-item" href="{{route('web-design')}}">Web Design</a>
                                        <a class="dropdown-item dropdown-toggle borderNone" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Dropdown
                                        </a>
                                        <div class="dropdown-menu dropdownMenu nextDropdown" aria-labelledby="navbarDropdown1">
                                            <a class="dropdown-item" href="#">Page 1</a>
                                            <a class="dropdown-item" href="#">Page 2</a>
                                            <a class="dropdown-item" href="#">Page 3</a>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('about-us')}}">about us</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('contact-us')}}">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    <a href="{{route('contact-us-form')}}" class="btn btn-red ml-2 ml-lg-6">get quote <span class="icon-arrow arrow-btn"></span></a>
                </div>
            </div>
        </div>
    </div>
