@extends('web.layouts.web')
@section('title','contact-us form')
@section('main')
    <main>
        <div class="contactSec py-7 py-lg-10">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10 col-lg-8">
                        <form action="#" class="contactsupport">
                            <div class="form-group">
                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email Address">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Number">
                            </div>
                            <div class="form-group mb-5">
                                <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Company Name">
                            </div>
                            <div class="form-group mb-5">
                                <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Website URL">
                            </div>
                            <div class="form-group">
                                <select class="form-control" id="exampleFormControlSelect1">
                                    <option value="" selected="selected">Select Project Budget</option>
                                    <option value="Below $5000">Below $5K</option>
                                    <option value="$5K - $15K">$5K - $15K</option>
                                    <option value="$15K - $40K">$15K - $40K</option>
                                    <option value="$40K - $60K">$40K - $60K</option>
                                    <option value="$60K and above">$60K and above</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <select class="form-control" id="exampleFormControlSelect1">
                                    <option value="" selected="selected">Select Some Option</option>
                                    <option>App Development</option>
                                    <option>Web development</option>
                                    <option>Web Design</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-red mx-auto">Request a quote <span class="icon-arrow arrow-btn"></span></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
