@extends('web.layouts.web')
@section('title','web development')
@section('main')

    <main>
        <div class="pageBanner">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <div class="bannerCaption text-white">
                            <span class="bannerTitle d-block mb-1">Web Design Company</span>
                            <h1 class="text-white">Website Design</h1>
                            <p>StartDesigns gives inventive web design services that convey your business more effectively online to visitors for the first time. We have an in-house team of web designers with 10+ years of experience with a 95% success rate in all industries. Start Designs giving creative and responsive website design solutions that enhance the user experience from every platform to convert your potential customers in your client.</p>
                            <div class="btnHolder d-flex pt-2 pt-lg-3">
                                <a href="#" class="btn btn-red mr-4">contact support <span class="icon-arrow arrow-btn"></span></a>
                                <a href="#" class="btn btn-yellow">start project <span class="icon-arrow arrow-btn"></span></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="imgHolder">
                            <img src="{{asset('web/images/webb-designn-bennr.png')}}" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="serviceSec mb-2 mb-lg-4 pt-7 pt-lg-10">
            <div class="container">
                <h2 class="text-capitalize text-center mb-6">We Offer Website <span class="headingColor">Design Services</span></h2>
                <div class="row justify-content-center">
                    <div class="col-md-6 col-lg-4">
                        <a href="#" class="serviceCol d-block text-center">
                            <div class="ico">
                                <img src="{{asset('web/images/custom-web-design.webp')}}" class="img-fluid">
                            </div>
                            <div class="txt">
                                <h5 class="text-capitalize">Custom Website Design</h5>
                                <p>Start Designs giving creative and responsive website design solutions that enhance the user experience from every platform to convert your potential customers in your client.</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <a href="#" class="serviceCol d-block text-center">
                            <div class="ico">
                                <img src="{{asset('web/images/ecommerce-web-design.webp')}}" class="img-fluid">
                            </div>
                            <div class="txt">
                                <h5 class="text-capitalize">eCommerce Website Design</h5>
                                <p>Start Designs giving creative and responsive website design solutions that enhance the user experience from every platform to convert your potential customers in your client.</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <a href="#" class="serviceCol d-block text-center">
                            <div class="ico">
                                <img src="{{asset('web/images/responsive-web-design.webp')}}" class="img-fluid">
                            </div>
                            <div class="txt">
                                <h5 class="text-capitalize">Responsive Website Design</h5>
                                <p>Start Designs giving creative and responsive website design solutions that enhance the user experience from every platform to convert your potential customers in your client.</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <a href="#" class="serviceCol d-block text-center">
                            <div class="ico">
                                <img src="{{asset('web/images/wordpress-dev-design.webp')}}" class="img-fluid">
                            </div>
                            <div class="txt">
                                <h5 class="text-capitalize">wordpress Website Design</h5>
                                <p>Start Designs giving creative and responsive website design solutions that enhance the user experience from every platform to convert your potential customers in your client.</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="ChooseSec mb-7 mb-lg-10 pt-7 pt-lg-10">
            <div class="container">
                <h2 class="text-capitalize text-center mb-6 text-white"><span>Why Choose Our Website Design Services In australia ?</span></h2>
                <div class="row justify-content-center">
                    <div class="col-md-6">
                        <div class="chooseCol animationCol" style="background: #fff;">
                            <div class="ico">
                                <img src="{{asset('web/images/team.svg')}}" class="img-fluid">
                            </div>
                            <div class="txt">
                                <h3 class="text-capitalize">Experience Team</h3>
                                <p>Our experienced team is the key to success that handle any project impressively. Our experience helps us to complete our professional goals in time without any hurdles.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="chooseCol animationCol" style="background: #fff;">
                            <div class="ico">
                                <img src="{{asset('web/images/on-time.svg')}}" class="img-fluid">
                            </div>
                            <div class="txt">
                                <h3 class="text-capitalize">On-time Delivery</h3>
                                <p>While we having a team of highly professional developers and experts we able to deliver projects on time. Without any comprise in the quality of products and services. We do bug-free codding.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="chooseCol animationCol" style="background: #fff;">
                            <div class="ico">
                                <img src="{{asset('web/images/creative-idea.svg')}}" class="img-fluid">
                            </div>
                            <div class="txt">
                                <h3 class="text-capitalize">Creative Ideas</h3>
                                <p>Being a leading website design company we have the reputation of designing the most inspiring and creative ideas that help customers to build a unique identity through the online presence in the industry.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="chooseCol animationCol" style="background: #fff;">
                            <div class="ico">
                                <img src="{{asset('web/images/approch.svg')}}" class="img-fluid">
                            </div>
                            <div class="txt">
                                <h3 class="text-capitalize">Professional Approach</h3>
                                <p>Our approach is more professional. We always listen to the requirements and ideas of our customers. Share our experience to improve and provides that our customer thinks.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="chooseCol animationCol" style="background: #fff;">
                            <div class="ico">
                                <img src="{{asset('web/images/nda.svg')}}" class="img-fluid">
                            </div>
                            <div class="txt">
                                <h3 class="text-capitalize">Non Disclosure Agreement (NDA) Policy</h3>
                                <p>We are strictly concern about our client data, we make it confidential to provide more securities to our customers. We always follow the NDA policy.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="chooseCol animationCol" style="background: #fff;">
                            <div class="ico">
                                <img src="{{asset('web/images/support.svg')}}" class="img-fluid">
                            </div>
                            <div class="txt">
                                <h3 class="text-capitalize">Customer Support</h3>
                                <p>As an emerging eCommerce web designing company, we ensure to provides 24x7 customer support. We help clients to make familiar about using CMS, which we use to build an eCommerce website.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="questionSec mb-7 mb-lg-10">
            <div class="container">
                <h2 class="mb-4 mb-lg-6 text-black text-center">Frequently <span class="headingColor">Asked Questions</span></h2>
                <div class="accordion" id="accordionExample">
                    <div class="card cardBlock">
                        <div class="card-header cardHeader" id="headingOne">
                            <h3 class="mb-0">
                                <button class="btn btn-link accordionOpener" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                    <strong>What is the nearest Auto shop?</strong>
                                    <i class="icon-plus icn"></i>
                                </button>
                            </h3>
                        </div>
                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card-body">
                                <p>I searched on google and found that perthautorepair is the Best Auto repair near me.</p>
                            </div>
                        </div>
                    </div>
                    <div class="card cardBlock">
                        <div class="card-header cardHeader" id="headingTwo">
                            <h2 class="mb-0">
                                <button class="btn btn-link accordionOpener" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <strong>What is the nearest Auto shop?</strong>
                                    <i class="icon-plus icn"></i>
                                </button>
                            </h2>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                            <div class="card-body">
                                <p>As we are committed to provide quality repair service, it means we will only use high-standard, quality-tested and original spare parts for your vehicle.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="blogSec mb-2 mb-lg-10">
            <div class="container">
                <h2 class="text-capitalize text-center mb-6">Read The Tech Stories Of <span class="headingColor">The Week</span></h2>
                <div class="row justify-content-center">
                    <div class="col-md-6 col-lg-4">
                        <a href="" class="blogCol d-block">
                            <div class="imgHolder">
                                <img src="{{asset('web/images/blog-3.png')}}" class="img-fluid">
                            </div>
                            <div class="txt">
                                <span class="d-block text-uppercase title">WEB DESIGN, WEB DEVELOPMENT</span>
                                <h3 class="text-capitalize">Top Website Builders for Small Business: 2022</h3>
                                <p>Start Designs giving creative and responsive website design solutions that enhance the user experience from every platform to convert your potential customers in your client.</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <a href="" class="blogCol d-block">
                            <div class="imgHolder">
                                <img src="{{asset('web/images/blog-3.png')}}" class="img-fluid">
                            </div>
                            <div class="txt">
                                <span class="d-block text-uppercase title">WEB DESIGN, WEB DEVELOPMENT</span>
                                <h3 class="text-capitalize">Top Website Builders for Small Business: 2022</h3>
                                <p>Start Designs giving creative and responsive website design solutions that enhance the user experience from every platform to convert your potential customers in your client.</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <a href="" class="blogCol d-block">
                            <div class="imgHolder">
                                <img src="{{asset('web/images/blog-3.png')}}" class="img-fluid">
                            </div>
                            <div class="txt">
                                <span class="d-block text-uppercase title">Blogging, WEB DESIGN</span>
                                <h3 class="text-capitalize">Top Website Builders for Small Business: 2022</h3>
                                <p>Start Designs giving creative and responsive website design solutions that enhance the user experience from every platform to convert your potential customers in your client.</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
