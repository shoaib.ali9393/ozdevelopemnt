<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('web/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('web/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('web/css/responsive.css')}}">
    <title>{{config('app.name')}} - @yield('title')</title>
    @yield('style')
</head>
<body>
<div class="wrapper">
    <header id="pageHeader">
        @include('web.partial.header')
    </header>
    <main>
        @yield('main')
    </main>
    <footer>
        @include('web.partial.footer')
    </footer>
</div>


<script src="{{asset('web/js/jquery-3.6.1.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
@yield('js')
</body>
</html>
