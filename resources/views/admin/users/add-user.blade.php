@extends('admin.layouts.admin')
@section('title','add-user')
@section('section')
    <div class="container-fluid">
    @include('flash-messages')
    <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Add User</h6>
            </div>
            <div class="card-body">
                <form class="user" method="POST" action="{{route('admin.add.user.save')}}">
                    @csrf
                    <div class="form-group">
                        <input type="text" class="form-control form-control-user  @error('name') is-invalid @enderror"
                               id="name" aria-describedby="name" name="name"
                               placeholder="Enter Name..." value="{{ old('name') }}" required autocomplete="name"
                               autofocus>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control form-control-user  @error('email') is-invalid @enderror"
                               id="exampleInputEmail" aria-describedby="emailHelp" name="email"
                               placeholder="Enter Email Address..." value="{{ old('email') }}" required
                               autocomplete="email" autofocus>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="password"
                               class="form-control form-control-user @error('password') is-invalid @enderror"
                               id="exampleInputPassword" placeholder="Password" name="password" required
                               autocomplete="new-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="password"
                               class="form-control form-control-user @error('password_confirmation') is-invalid @enderror"
                               id="password_confirmation" placeholder="Confirm Password" name="password_confirmation"
                               required autocomplete="new-password">
                        @error('password_confirmation')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <select name="role" class="form-control exampleFormControlSelect1 @error('role') is-invalid @enderror"
                                id="exampleFormControlSelect1" required>
                            <option value="">Select Role</option>
                            <option value="{{\App\Models\User::USER}}">{{\App\Models\User::USER}}</option>
                            <option value="{{\App\Models\User::ADMIN}}">{{\App\Models\User::ADMIN}}</option>
                            <option value="{{\App\Models\User::EMPLOYEE}}">{{\App\Models\User::EMPLOYEE}}</option>
                        </select>
                        @error('role')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <button type="sumbit" class="btn btn-primary btn-user btn-block">
                        Save
                    </button>
                    <hr>
                </form>
            </div>
        </div>

    </div>
@endsection

@section('style')

@endsection

@section('js')

@endsection
