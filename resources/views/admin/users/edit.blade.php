@extends('admin.layouts.admin')
@section('title','edit-user')
@section('section')
    <div class="container-fluid">
    @include('flash-messages')
    <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Add User</h6>
            </div>
            <div class="card-body">
                <form class="user" method="POST" action="{{route('admin.user.update')}}">
                    @csrf
                    <input type="hidden" name="id" value="{{$user->id}}">
                    <div class="form-group">
                        <input type="text" class="form-control form-control-user  @error('name') is-invalid @enderror"
                               id="name" aria-describedby="name" name="name"
                               placeholder="Enter Name..." value="{{$user->name}}" required autocomplete="name"
                               autofocus>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control form-control-user  @error('email') is-invalid @enderror"
                               id="exampleInputEmail" aria-describedby="emailHelp" name="email"
                               placeholder="Enter Email Address..." value="{{$user->email}}" required
                               autocomplete="email" autofocus>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="password"
                               class="form-control form-control-user @error('password') is-invalid @enderror"
                               id="exampleInputPassword" placeholder="New Password" name="password"
                               autocomplete="new-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="password"
                               class="form-control form-control-user @error('password_confirmation') is-invalid @enderror"
                               id="password_confirmation" placeholder="Confirm Password" name="password_confirmation"
                               autocomplete="new-password">
                        @error('password_confirmation')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <select name="role"
                                class="form-control exampleFormControlSelect1 @error('role') is-invalid @enderror"
                                id="exampleFormControlSelect1" required>
                            <option value="">Select Role</option>
                            <option
                                value="{{\App\Models\User::USER}}" {{($user->role == \App\Models\User::USER)?"selected":""}}>{{\App\Models\User::USER}}</option>
                            <option
                                value="{{\App\Models\User::ADMIN}}" {{($user->role == \App\Models\User::ADMIN)?"selected":""}}>{{\App\Models\User::ADMIN}}</option>
                            <option
                                value="{{\App\Models\User::EMPLOYEE}}" {{($user->role == \App\Models\User::EMPLOYEE)?"selected":""}}>{{\App\Models\User::EMPLOYEE}}</option>
                        </select>
                        @error('role')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <button type="sumbit" class="btn btn-primary btn-user btn-block">
                        Update
                    </button>
                    <hr>
                </form>
            </div>
        </div>

    </div>
@endsection

@section('style')

@endsection

@section('js')

@endsection
