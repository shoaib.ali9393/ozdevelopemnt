@extends('admin.layouts.admin')
@section('title','Web-general-Setting')
@section('section')
    <div class="container-fluid">
    @include('flash-messages')
    <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Add Faq</h6>
            </div>
            <div class="card-body">
                <form class="user" method="POST" action="{{route('admin.faq.save')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="col-md-16 mt-4 w-50">
                        <select name="page"
                                class="form-control exampleFormControlSelect1 @error('index_status') is-invalid @enderror"
                                id="exampleFormControlSelect1" required>
                            <option value="">Select page</option>
                            @foreach($pages as $page)
                                <option value="{{$page->id}}">{{$page->page}}</option>
                            @endforeach
                        </select>
                        @error('index_status')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="col-md-10 mt-4" >
                        <label for="faq">FAQ Blue Title</label>
                        <div class="input-group-prepend mt-1">
                            <input type="text" name="blue_title" class="form-control" placeholder="Enter title.." value="">
                        </div>
                    </div>
                    <div class="col-md-10 mt-4 " >
                        <label for="faq">FAQ Blue Title</label>
                        <div class="input-group-prepend  mt-1">
                            <input type="text" name="red_title" class="form-control" placeholder="Enter title.." value="">                        </div>
                    </div>
                    <div class="col-md-10 mt-4 rule" >
                        <label for="faq">FAQ</label>
                        <div class="input-group-prepend faq-class mt-1 d-block">
                            <input type="text" name="question[]" class="form-control" placeholder="Enter Question.." value="">
                            <input type="text" name="answer[]" class="form-control mt-2" placeholder="Enter Answer" value="">
                            <div class="fm-rulebutton ml-1 mt-2">
                                <input type="button" id="addButton" class='add' value="+"/>
                            </div>
                            <div class="fm-opt ml-1 mt-2" >
                                <input type="button" class='remove d-none' value="-"/>
                            </div>
                        </div>
                    </div>
                    <button type="sumbit" class="btn btn-primary btn-user btn-block mt-4">
                        Save
                    </button>
                    <hr>
                </form>
            </div>
        </div>

    </div>
@endsection

@section('style')

@endsection

@section('js')
    <script>
        $(document).ready(function() {
            var id = 0;
            $('.add').click(function(){
                id++;
                var prot = $(document).find(".rule .faq-class:first").clone();
                prot.attr("class", 'faq-class'+id+' input-group-prepend mt-1 d-block')
                prot.find(".remove").attr("data-set",'faq-class'+id+'');
                prot.find('.remove').removeClass('d-none');
                prot.find('.add').addClass('d-none');
                $(document).find(".rule").append(prot);
            });
        });
        $(document).on('click','.remove' , function () {
            var removeClass = $(this).attr('data-set');
            $('.'+removeClass).remove();
        })


    </script>
@endsection
