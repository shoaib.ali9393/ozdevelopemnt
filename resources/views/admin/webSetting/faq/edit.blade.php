@extends('admin.layouts.admin')
@section('title','Web-general-Setting')
@section('section')
    <div class="container-fluid">
    @include('flash-messages')
    <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">General settings</h6>
            </div>
            <div class="card-body">
                <form class="user" method="POST" action="{{route('admin.save.general.setting')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="col-md-10 mt-4" >
                        <label for="faq">FAQ Blue Title</label>
                        <div class="input-group-prepend mt-1">
                            <input type="text" name="blue_title" class="form-control" placeholder="Enter title.." value="{{}}">
                        </div>
                    </div>
                    <div class="col-md-10 mt-4 " >
                        <label for="faq">FAQ Blue Title</label>
                        <div class="input-group-prepend  mt-1">
                            <input type="text" name="red_title" class="form-control" placeholder="Enter title.." value="">                        </div>
                    </div>
                    <div class="col-md-10 mt-4 rule" >
                        <label for="social_icon">FAQ</label>
                        @foreach(json_decode($configration->social_icon) as $key=>$socialIcon)
                            <div class="input-group-prepend social-icons-class mt-4 remove-socaial{{$loop->iteration}}">
                                <input type="text" name="social_icon_links[]" class="form-control" placeholder="Socail Link" value="{{$key}}">
                                <input type="text" name="social_icons[]" class="form-control" placeholder="Social Icon" value="{{$socialIcon}}">
                                <div class="fm-rulebutton ml-5">
                                    <input type="button" id="addButton" class='add {{($loop->iteration == 1 ? '' : "d-none")}}' value="+"/>
                                </div>
                                <div class="fm-opt ml-2" >
                                    <input type="button" class='remove {{($loop->iteration == 1 ? 'd-none' : "")}}' data-set="remove-socaial{{$loop->iteration}}" value="-"/>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <div class="col-md-10 mt-4 address-container" >
                        <label for="address">Address</label>
                        @foreach( json_decode($configration->address) as $key=>$address)
                            <div class="input-group-prepend address-container-class mt-4 remove-address-class{{$loop->iteration}}">
                                <input type="text" name="address[]" class="form-control" placeholder="Enter Address..." value="{{$key}}">
                                <input type="text" name="address_titles[]" class="form-control" placeholder="Enter Address title..." value="{{$address}}">
                                <div class="fm-rulebutton ml-5">
                                    <input type="button" id="address-addButton" class='address-add  {{($loop->iteration == 1 ? '' : "d-none")}}' value="+"/>
                                </div>
                                <div class="fm-opt ml-2" >
                                    <input type="button" class='address-remove {{($loop->iteration == 1 ? 'd-none' : "")}}' data-set="remove-address-class{{$loop->iteration}}" value="-"/>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <button type="sumbit" class="btn btn-primary btn-user btn-block mt-4">
                        Save
                    </button>
                    <hr>
                </form>
            </div>
        </div>

    </div>
@endsection

@section('style')

@endsection

@section('js')
    <script>
        $(document).ready(function() {
            var id = 0;
            $('.add').click(function(){
                id++;
                var prot = $(document).find(".rule .social-icons-class:first").clone();
                prot.attr("class", 'social-icons-class'+id+' input-group-prepend mt-4')
                prot.find(".remove").attr("data-set",'social-icons-class'+id+'');
                prot.find('.remove').removeClass('d-none');
                prot.find('.add').addClass('d-none');
                $(document).find(".rule").append(prot);
            });
        });
        $(document).on('click','.remove' , function () {
            var removeClass = $(this).attr('data-set');
            $('.'+removeClass).remove();
        })


    </script>
@endsection
