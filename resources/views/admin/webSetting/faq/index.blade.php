@extends('admin.layouts.admin')
@section('title','faq')
@section('section')
    <div class="container-fluid">

    @include('flash-messages')
    <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">FAQ List</h6>
                <a href="{{route('admin.faq.add')}}" class="btn btn-primary btn-icon-split float-right">
                                        <span class="icon text-white-50">
                                            <i class="fa fa-plus"></i>
                                        </span>
                    <span class="text">Add faq</span>
                </a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Page</th>
                            <th>created_at</th>
                            <th>updated_at</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($faqs as $key=>$faq)
                            <tr>
                                <td>{{$faq->page->page}}</td>
                                <td>{{$faq->created_at->format('m/d/Y')}}</td>
                                <td>{{$faq->updated_at->format('m/d/Y')}}</td>
                                <td>
{{--                                    <a href="{{ route('admin.user.delete',[$user->id]) }}"--}}
{{--                                       class="btn btn-danger btn-circle">--}}
{{--                                        <i class="fas fa-trash"></i>--}}
{{--                                    </a>--}}
                                    <a href="{{ route('admin.faq.edit',[$faq->id]) }}"
                                       class="btn btn-danger btn-circle ">
                                        <i class="fas fa-user-edit ml-1"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('style')
    <!-- Custom styles for this page -->
    <link href="{{asset('admin/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@endsection

@section('js')
    <!-- Page level plugins -->
    <script src="{{asset('admin/vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <!-- Page level custom scripts -->
    <script src="{{asset('admin/js/demo/datatables-demo.js')}}"></script>
@endsection
