@extends('admin.layouts.admin')
@section('title','Web-general-Setting')
@section('section')
    <div class="container-fluid">
    @include('flash-messages')
    <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">General settings</h6>
            </div>
            <div class="card-body">
                <form class="user" method="POST" action="{{route('admin.save.general.setting')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="col-md-12">
                        <label for="logo_image">Header logo</label>
                        <div class="input-group-prepend">
                            <input type="file" class="form-control" name="header_logo">
                            <input type="text" name="header_logo_name" class="form-control" placeholder="Image name" value="">
                            <input type="text" name="header_logo_alt" class="form-control" placeholder="Image alt" value="">
                        </div>
                        @error('header_logo_name')
                        <span style="display: block !important;" class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="col-md-12 mt-4">
                        <label for="logo_image">Footer logo</label>
                        <div class="input-group-prepend">
                            <input type="file" class="form-control" name="footer_logo">
                            <input type="text" name="footer_logo_name" class="form-control" placeholder="Image name" value="">
                            <input type="text" name="footer_logo_alt" class="form-control" placeholder="Image alt" value="">
                        </div>
                        @error('footer_logo_name')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="col-md-12 mt-4">
                        <label for="email">Email</label>
                        <div class="input-group-prepend">
                            <input type="email" name="email"  id="email" class="form-control" placeholder="Enter email" value="">
                        </div>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="col-md-12 mt-4">
                        <label for="phone">Contact</label>
                        <div class="input-group-prepend">
                            <input type="text" name="phone" id="phone" class="form-control" placeholder="Enter contact number" value="">
                        </div>
                        @error('phone')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="col-md-12 mt-4">
                        <label for="copy_right">Copy Right</label>
                        <div class="input-group-prepend">
                            <input type="text" name="copy_right" id="copy_right" class="form-control" placeholder="Enter copy right text..." value="">
                        </div>
                        @error('phone')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="col-md-12 mt-4">
                        <label for="working_hours">Working Time</label>
                        <div class="input-group-prepend">
                            <input type="text" name="working_hours" id="working_hours" class="form-control" placeholder="Enter working time text..." value="">
                        </div>
                        @error('phone')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="col-md-12 mt-4">
                        <label for="logo_image">Header Links</label>
                       <textarea name="header_links"></textarea>
                    </div>
                    <div class="col-md-12 mt-4">
                        <label for="logo_image">Footer Links</label>
                        <textarea name="footer_links" class=""></textarea>
                    </div>
                    <div class="col-md-12 mt-4">
                        <label for="map">Map</label>
                        <textarea name="map" id="map" class=""></textarea>
                    </div>

                    <div class="col-md-10 mt-4 rule" >
                        <label for="social_icon">Social Icon</label>
                        <div class="input-group-prepend social-icons-class mt-4">
                            <input type="text" name="social_icon_links[]" class="form-control" placeholder="Socail Link" value="">
                            <input type="text" name="social_icons[]" class="form-control" placeholder="Social Icon" value="">
                            <div class="fm-rulebutton ml-5">
                                <input type="button" id="addButton" class='add' value="+"/>
                            </div>
                            <div class="fm-opt ml-2" >
                                <input type="button" class='remove d-none' value="-"/>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-10 mt-4 address-container" >
                        <label for="address">Address</label>
                        <div class="input-group-prepend address-container-class mt-4">
                            <input type="text" name="address[]" class="form-control" placeholder="Enter Address..." value="">
                            <input type="text" name="address_titles[]" class="form-control" placeholder="Enter Address title..." value="">
                            <div class="fm-rulebutton ml-5">
                                <input type="button" id="address-addButton" class='address-add' value="+"/>
                            </div>
                            <div class="fm-opt ml-2" >
                                <input type="button" class='address-remove d-none' value="-"/>
                            </div>
                        </div>
                    </div>
                    <button type="sumbit" class="btn btn-primary btn-user btn-block mt-4">
                        Save
                    </button>
                    <hr>
                </form>
            </div>
        </div>

    </div>
@endsection

@section('style')

@endsection

@section('js')
<script>
    $(document).ready(function() {
        var id = 0;
        $('.add').click(function(){
            id++;
            var prot = $(document).find(".rule .social-icons-class:first").clone();
            prot.attr("class", 'social-icons-class'+id+' input-group-prepend mt-4')
            prot.find(".remove").attr("data-set",'social-icons-class'+id+'');
            prot.find('.remove').removeClass('d-none');
            prot.find('.add').addClass('d-none');
            $(document).find(".rule").append(prot);
        });
    });
    $(document).on('click','.remove' , function () {
        var removeClass = $(this).attr('data-set');
        $('.'+removeClass).remove();
    })



    $(document).ready(function() {
        var id = 0;
        $('.address-add').click(function(){
            id++;
            var prot = $(document).find(".address-container .address-container-class:first").clone();
            prot.attr("class", 'address-container-class'+id+' input-group-prepend mt-4')
            prot.find(".address-remove").attr("data-set",'address-container-class'+id+'');
            prot.find('.address-remove').removeClass('d-none');
            prot.find('.address-add').addClass('d-none');
            $(document).find(".address-container").append(prot);
        });
    });
    $(document).on('click','.address-remove' , function () {
        var removeClass = $(this).attr('data-set');
        $('.'+removeClass).remove();
    })

</script>
@endsection
