@extends('admin.layouts.admin')
@section('title','Home page list')
@section('section')
    <div class="container-fluid">
    @include('flash-messages')
    <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Home</h6>
                @if(isset($home))
                    <a href="{{route('admin.edit.home.page')}}" class="btn btn-primary btn-icon-split float-right">
                                        <span class="icon text-white-50">
                                            <i class="fa fa-plus"></i>
                                        </span>
                        <span class="text">Edit Data</span>
                    </a>
                @else
                    <a href="{{route('admin.add.home.page')}}" class="btn btn-primary btn-icon-split float-right">
                                        <span class="icon text-white-50">
                                            <i class="fa fa-plus"></i>
                                        </span>
                        <span class="text">Add Data</span>
                    </a>
                @endif
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>name</th>
                            <th>Index Status</th>
                            <th>Status</th>
                            <th>Created Date</th>
                            <th>Updated Date</th>
                        </tr>
                        </thead>

                        <tbody>
                        @if(isset($home))
                        <tr>
                            <td>#{{$home->id}}</td>
                            <td>{{$home->page}}</td>
                            <td>{{$home->index_status}}</td>
                            <td>{{$home->status}}</td>
                            <td>{{$home->created_at->format('m/d/Y')}}</td>
                            <td>{{$home->updated_at->format('m/d/Y')}}</td>
                        </tr>
                         @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('style')
    <!-- Custom styles for this page -->
    <link href="{{asset('admin/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@endsection

@section('js')
    <!-- Page level plugins -->
    <script src="{{asset('admin/vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <!-- Page level custom scripts -->
    <script src="{{asset('admin/js/demo/datatables-demo.js')}}"></script>
@endsection
