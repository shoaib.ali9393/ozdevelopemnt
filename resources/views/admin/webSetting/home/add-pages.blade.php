@extends('admin.layouts.admin')
@section('title','add home page')
@section('section')
    <div class="container-fluid">
    @include('flash-messages')
    <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Home Page</h6>
            </div>
            <div class="card-body">
                <form class="user" method="POST" action="{{route('admin.save.page.data')}}"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="col-md-12 mt-4">
                        <select name="index_status"
                                class="form-control exampleFormControlSelect1 @error('index_status') is-invalid @enderror"
                                id="exampleFormControlSelect1" required>
                            <option value="">Select Index</option>
                            <option value="1">Index</option>
                            <option value="0">No Index</option>
                        </select>
                        @error('index_status')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="col-md-12 mt-4">
                        <select name="page_status"
                                class="form-control exampleFormControlSelect1 @error('page_status') is-invalid @enderror"
                                id="exampleFormControlSelect1" required>
                            <option value="">Select Status</option>
                            <option value="1">Active</option>
                            <option value="0">Un-Active</option>
                        </select>
                        @error('page_status')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="col-md-12 mt-4">
                        <label for="page_name">Page Name</label>
                        <div class="input-group-prepend">
                            <input type="text" name="page_name" id="page_name" class="form-control"
                                   placeholder="Enter page name.." value="{{old('page_name')}}" required>
                        </div>
                        @error('page_name')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="col-md-12 mt-4">
                        <label for="meta_title">Meta Title</label>
                        <div class="input-group-prepend">
                            <input type="text" name="meta_title" id="meta_title" class="form-control"
                                   placeholder="Enter meta title.." value="{{old('meta_title')}}" required>
                        </div>
                        @error('meta_title')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="col-md-12 mt-4">
                        <label for="meta_description">Meta Description</label>
                        <div class="input-group-prepend">
                            <input type="text" name="meta_description" id="meta_description" class="form-control"
                                   placeholder="Enter meta description.." value="{{old('meta_description')}}"
                                   required>
                        </div>
                        @error('meta_description')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="d-flex">
                        <div class="col-md-6 mt-4">
                            <label for="banner_short_heading">Banner short heading</label>
                            <div class="input-group-prepend">
                                <input type="text" name="banner[short_heading]" id="banner_short_heading"
                                       class="form-control"
                                       placeholder="Enter banner short heading..."
                                       value="{{old('banner.short_heading')}}"
                                >
                            </div>
                        </div>
                        <div class="col-md-6 mt-4">
                            <label for="banner_main_heading">Banner main heading</label>
                            <div class="input-group-prepend">
                                <input type="text" name="banner[main_heading]" id="banner_main_heading"
                                       class="form-control"
                                       placeholder="Enter banner main heading..."
                                       value="{{old('banner.main_heading')}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 mt-4">
                        <label for="banner_heading_description">Banner heading description</label>
                        <div class="input-group-prepend">
                            <textarea name="banner[heading_description]">{{old('banner.heading_description')}}</textarea>
                        </div>
                    </div>
                    <div class="col-md-12 mt-4">
                        <label>Form Description</label>
                        <textarea class="form-control" id="form_description" name="form_description">{!! old('form_description') !!}</textarea>
                    </div>
                    <div class="d-flex">
                        <div class="col-md-6 mt-4">
                            <label for="after_service_black_heading">Black Heading Text (after service)</label>
                            <div class="input-group-prepend">
                                <input type="text" name="after_service[black_heading]" id="after_service_black_heading" class="form-control"
                                       placeholder="Enter heading..." value="{{old('after_service.black_heading')}}"
                                >
                            </div>
                        </div>
                        <div class="col-md-6 mt-4">
                            <label for="after_service_red_heading">Red Heading Text (after service)</label>
                            <div class="input-group-prepend">
                                <input type="text" name="after_service[red_heading]" id="after_service_red_heading" class="form-control"
                                       placeholder="Enter heading..." value="{{old('after_service.red_heading')}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 mt-4">
                        <label for="after_service_image">Image (after service)</label>
                        <div class="input-group-prepend">
                            <input type="file" class="form-control" name="after_service[image]" value="">
                            <input type="text" name="after_service[image_name]" class="form-control" placeholder="Image name" value="{{old('after_service.image_name')}}">
                            <input type="text" name="after_service[image_alt]" class="form-control" placeholder="Image alt" value="{{old('after_service.image_alt')}}">
                            <input type="hidden" name="after_service[image_id]" class="form-control"  value="{{old('after_service.image_id')}}">
                        </div>
                        @error('after_service_image_name')
                        <span style="display: block !important;" class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                        @error('after_service_image_alt')
                        <span style="display: block !important;" class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="col-md-12 mt-4">
                        <label for="after_service_description">Description (after service)</label>
                        <div class="input-group-prepend">
                            <textarea name="after_service[description]">{{old('after_service.description')}}</textarea>
                        </div>
                    </div>


                    <div class="d-flex">
                        <div class="col-md-6 mt-4">
                            <label for="before_faq_black_heading">Black Heading Text (Before FAQ)</label>
                            <div class="input-group-prepend">
                                <input type="text" name="before_faq[black_heading]" id="before_faq_black_heading" class="form-control"
                                       placeholder="Enter heading..." value="{{old('before_faq.black_heading')}}"
                                >
                            </div>
                        </div>
                        <div class="col-md-6 mt-4">
                            <label for="before_faq_red_heading">Red Heading Text (Before FAQ)</label>
                            <div class="input-group-prepend">
                                <input type="text" name="before_faq[red_heading]" id="before_faq_red_heading" class="form-control"
                                       placeholder="Enter heading..." value="{{old('before_faq.red_heading')}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 mt-4">
                        <label for="before_faq_image">Image (Before FAQ)</label>
                        <div class="input-group-prepend">
                            <input type="file" class="form-control" name="before_faq[image]" value="">
                            <input type="text" name="before_faq[image_name]" class="form-control" placeholder="Image name" value="{{old('before_faq.image_name')}}">
                            <input type="text" name="before_faq[image_alt]" class="form-control" placeholder="Image alt" value="{{old('before_faq.image_alt')}}">
                            <input type="hidden" name="before_faq[image_id]" class="form-control"  value="{{old('before_faq.image_id')}}">
                        </div>
                        @error('before_faq_image_name')
                        <span style="display: block !important;" class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                        @error('before_faq_image_alt')
                        <span style="display: block !important;" class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="col-md-12 mt-4">
                        <label for="before_faq_description">Description (Before FAQ)</label>
                        <div class="input-group-prepend">
                            <textarea name="before_faq[description]">{{old('before_faq.description')}}</textarea>
                        </div>
                    </div>
                    <button type="sumbit" class="btn btn-primary col-md-12 btn-user btn-block mt-4">
                        Save
                    </button>
                    <hr>
                </form>
            </div>
        </div>

    </div>
@endsection

@section('style')

@endsection

@section('js')
    <script>
        ClassicEditor
            .create( document.querySelector( '#form_description' ) )
            .catch( error => {
                console.error( error );
            } );
    </script>

@endsection
