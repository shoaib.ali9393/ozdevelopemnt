@extends('admin.layouts.admin')
@section('title','Smtp Setting')
@section('section')
    <div class="container-fluid">
    @include('flash-messages')
    <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">SMTP settings</h6>
            </div>
            <div class="card-body">
                <form class="user" method="POST" action="{{route('admin.edit.save.smtp')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="col-md-6 mt-4">
                        <label for="mail_mailer">Mail Mailer</label>
                        <div class="input-group-prepend">
                            <input type="text" name="mail_mailer"  id="mail_mailer" class="form-control" placeholder="Enter mailer" value="{{isset($smtp->mail_mailer)?$smtp->mail_mailer:Null}}" required>
                        </div>
                        @error('mail_mailer')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="col-md-6 mt-4">
                        <label for="mail_host">Email Host</label>
                        <div class="input-group-prepend">
                            <input type="email" name="mail_host"  id="mail_host" class="form-control" placeholder="Enter email" value="{{isset($smtp->mail_host) ? $smtp->mail_host : Null}}" required>
                        </div>
                        @error('mail_host')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="col-md-6 mt-4">
                        <label for="mail_port">Email Port</label>
                        <div class="input-group-prepend">
                            <input type="number" name="mail_port"  id="mail_port" class="form-control" placeholder="Enter port" value="{{isset($smtp->mail_port) ? $smtp->mail_port : Null}}" required>
                        </div>
                        @error('mail_port')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="col-md-6 mt-4">
                        <label for="mail_username">Email Username</label>
                        <div class="input-group-prepend">
                            <input type="text" name="mail_username"  id="mail_username" class="form-control" placeholder="Enter email" value="{{isset($smtp->mail_username) ? $smtp->mail_username :Null}}" required>
                        </div>
                        @error('mail_username')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="col-md-6 mt-4">
                        <label for="mail_password">Email Password</label>
                        <div class="input-group-prepend">
                            <input type="text" name="mail_password"  id="mail_password" class="form-control" placeholder="Enter password" value="{{isset($smtp->mail_password) ? $smtp->mail_password : Null}}" required>
                        </div>
                        @error('mail_password')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="col-md-6 mt-4">
                        <label for="mail_encryption">Email Encryption</label>
                        <div class="input-group-prepend">
                            <input type="text" name="mail_encryption"  id="mail_encryption" class="form-control" placeholder="Enter encryption" value="{{isset($smtp->mail_encryption) ? $smtp->mail_encryption :Null}}" required>
                        </div>
                        @error('mail_encryption')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="col-md-6 mt-4">
                        <label for="mail_from_address">Email From address</label>
                        <div class="input-group-prepend">
                            <input type="text" name="mail_from_address"  id="mail_from_address" class="form-control" placeholder="Enter email" value="{{isset($smtp->mail_from_address) ? $smtp->mail_from_address : Null}}" required>
                        </div>
                        @error('mail_from_address')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="col-md-6 mt-4">
                        <label for="mail_from_name">Email From name</label>
                        <div class="input-group-prepend">
                            <input type="text" name="mail_from_name"  id="mail_from_name" class="form-control" placeholder="Enter name" value="{{isset($smtp->mail_from_name) ? $smtp->mail_from_name :Null}}" required>
                        </div>
                        @error('mail_from_name')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>

                    <button type="sumbit" class="btn btn-primary col-md-6 btn-user btn-block mt-4">
                        Save
                    </button>
                    <hr>
                </form>
            </div>
        </div>

    </div>
@endsection

@section('style')

@endsection

@section('js')

@endsection
