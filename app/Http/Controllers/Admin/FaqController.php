<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\PageData;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    function index()
    {
        $faqs = PageData::with('page')->get();
        return view('admin.webSetting.faq.index' , compact('faqs'));
    }

    function addFaq()
    {
        $pages = Page::select('page', 'id')->get();
        return view('admin.webSetting.faq.addfaq', compact('pages'));
    }

    function save(Request $request)
    {
        $faq=[];
        $data=[];
        foreach ($request->question as $key=>$question)
        {
         $faq[$question] = $request->answer[$key];
        }
        $data["blue_title"] = $request->blue_title;
        $data["red_title"] = $request->red_title;
        $data["faq"] = $faq;
        PageData::create([
           'page_id' => $request->page,
           'faq' => json_encode($data)
        ]);

        return redirect()->route('admin.faq.list')->with('success', 'Data added!');

    }


    function editFaq(Request $request)
    {
        $faq = PageData::where('id' , $request->id)->first();
    }
}
