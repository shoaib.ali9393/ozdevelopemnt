<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\HelperController;
use App\Models\Gallery;
use App\Models\Page;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HomePageController extends Controller
{
    public $helperFunctions;

    public function __construct(HelperController $helperFunctions)
    {
        $this->helperFunctions = $helperFunctions;
    }

    function index()
    {
        $home = Page::where('page', 'like', '%' . Page::HOME . '%')->first();
        return view('admin.webSetting.home.list', compact('home'));
    }

    function addHomepage()
    {
        return view('admin.webSetting.home.add-pages');
    }

    function saveData(Request $request)
    {
        $Data = $request->except('_token'
            , 'after_service.image_alt'
            , 'after_service.image_name'
            , 'after_service.image'
            , 'before_faq.image_alt'
            , 'before_faq.image_name'
            , 'before_faq.image'
        );
        $validator = Validator::make($request->all(), [
            'page_name' => 'required|unique:pages,page,',
            'after_service.image' => 'sometimes',
            'before_faq.image' => 'sometimes',
            'after_service.image_name' => ['required_with:after_service.image', 'unique:galleries,name'],
            'before_faq.image_name' => ['required_with:before_faq.image', 'unique:galleries,name'],
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        try {
            if (isset($request['after_service']["image"])) {
                $galleryData = $this->helperFunctions->ImageProcess(
                    $request['after_service']["image"]
                    , $request['after_service']["image_name"]
                    , $request['after_service']["image_alt"]
                    , Gallery::Image
                    , $request['after_service']['image_id']);
                $Data['after_service']['image_id'] = $galleryData->id;
                $Data['after_service']['full_image'] = $galleryData->image;
                $Data['after_service']['image_name'] = $galleryData->name;
                $Data['after_service']['image_alt'] = $galleryData->alt;
            }
            if (isset($request['before_faq']["image"])) {
                $galleryData = $this->helperFunctions->ImageProcess(
                    $request['before_faq']["image"]
                    , $request['before_faq']["image_name"]
                    , $request['before_faq']["image_alt"]
                    , Gallery::Image
                    , $request['before_faq']['image_id']);
                $Data['before_faq']['image_id'] = $galleryData->id;
                $Data['before_faq']['full_image'] = $galleryData->image;
                $Data['before_faq']['image_name'] = $galleryData->name;
                $Data['before_faq']['image_alt'] = $galleryData->alt;
            }
            Page::create([
                'page' => $Data['page_name'],
                'content' => (json_encode($Data)),
                'meta_title' => $request['meta_title'],
                'meta_description' => $request['meta_description'],
                'index_status' => $request['index_status'],
                'status' => $request['page_status'],
            ]);
            return redirect()->route('admin.home.page.index')->with('success', 'Data Added!');
        } catch (Exception $ex) {
            return redirect()->back()->with('error-message', 'Something went wrong please try again!');
        }
    }


    function editPage()
    {
        $home = Page::where('page', 'like', '%' . Page::HOME . '%')->first();
        $home->content = json_decode($home->content);
        return view('admin.webSetting.home.edit', compact('home'));
    }

    function updatePage(Request $request)
    {
        $Data = $request->except('_token');
        $validator = Validator::make($request->all(), [
            'page_name' => 'required|unique:pages,page,'.$request->page_id,
            'after_service.image' => 'sometimes',
            'before_faq.image' => 'sometimes',
            'after_service.image_name' => ['required_with:after_service.image', 'unique:galleries,name,'.$request->after_service['image_id']],
            'before_faq.image_name' => ['required_with:before_faq.image', 'unique:galleries,name,'.$request->before_faq['image_id']],
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        try {
            if (isset($request['after_service']['image'])) {
                $GalleryData = $this->helperFunctions->ImageProcess(
                    $request['after_service']['image']
                    , $request['after_service']['image_name']
                    , $request['after_service']['image_alt']
                    , Gallery::Image
                    , $request['after_service']['image_id']);
                $Data['after_service']['image_id'] = $GalleryData->id;
                $Data['after_service']['full_image'] = $GalleryData->image;
                $Data['after_service']['image_name'] = $GalleryData->name;
                $Data['after_service']['image_alt'] = $GalleryData->alt;
            }
            if (isset($request['before_faq']['image'])) {
                $beforeFaqGalleryData = $this->helperFunctions->ImageProcess(
                    $request['before_faq']['image']
                    , $request['before_faq']['image_name']
                    , $request['before_faq']['image_alt']
                    , Gallery::Image
                    , $request['before_faq']['image_id']);
                $Data['before_faq']['image_id'] = $beforeFaqGalleryData->id;
                $Data['before_faq']['full_image'] = $beforeFaqGalleryData->image;
                $Data['before_faq']['image_name'] = $beforeFaqGalleryData->name;
                $Data['before_faq']['image_alt'] = $beforeFaqGalleryData->alt;
            }
            Page::where('id',$request->page_id)->update([
                'page' => $Data['page_name'],
                'meta_title' => $request['meta_title'],
                'meta_description' => $request['meta_description'],
                'index_status' => $request['index_status'],
                'status' => $request['page_status'],
                'content' => json_encode($Data),
            ]);
            return redirect()->back()->with('success', 'Data Added!');
        } catch (Exception $ex) {
            return redirect()->back()->with('error-message', 'Something went wrong please try again!');
        }
    }
}
