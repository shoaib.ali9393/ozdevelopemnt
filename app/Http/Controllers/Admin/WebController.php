<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\HelperController;
use App\Models\Configration;
use App\Models\Gallery;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class WebController extends Controller
{

    public $helperFunctions;

    public function __construct(HelperController $helperFunctions)
    {
        $this->helperFunctions = $helperFunctions;
    }

    function index()
    {
        $configration = Configration::with(['header_logo', 'footer_logo'])->first();
        if (isset($configration)) {
            return view('admin.webSetting.general.edit', compact('configration'));
        }
        return view('admin.webSetting.general.index');
    }

    function saveGeneralSetting(Request $request)
    {
        $config = Configration::first();
        $headerLogoId = isset($config->header_logo_id) ? $config->header_logo_id : '';
        $footerLogoId = isset($config->footer_logo_id) ? $config->footer_logo_id : '';
        $validator = Validator::make($request->all(), [
            'header_logo' => ['sometimes'],
            'footer_logo' => ['sometimes'],
            'header_logo_name' => ['required_with:header_logo', 'unique:galleries,name,' . $headerLogoId],
            'footer_logo_name' => ['required_with:footer_logo', 'unique:galleries,name,' . $footerLogoId],
        ]);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)
                ->withInput();
        }
        try {
            $data_array = [
                'header_links' => json_encode($request->header_links),
                'footer_links' => json_encode($request->footer_links),
                'email' => $request->email,
                'phone' => $request->phone,
                'map' => json_encode($request->map),
                'copy_right' => $request->copy_right,
                'working_hours' => $request->working_hours,
            ];
            if (isset($request->header_logo)) {
                $header_logo = $this->helperFunctions->ImageProcess(
                    $request->header_logo
                    , $request->header_logo_name
                    , $request->header_logo_alt
                    , Gallery::Image
                    , isset($config->header_logo_id) ? $config->header_logo_id : null);
                $data_array['header_logo_id'] = $header_logo->id;
            }
            if (isset($request->footer_logo)) {
                $footer_logo = $this->helperFunctions->ImageProcess(
                    $request->footer_logo
                    , $request->footer_logo_name
                    , $request->footer_logo_alt
                    , Gallery::Image
                    , isset($config->footer_logo_id) ? $config->footer_logo_id : null);
                $data_array['footer_logo_id'] = $footer_logo->id;
            }

            $socialIconArray = [];
            $addressArray = [];

            foreach ($request->social_icon_links as $key => $social_icon_link) {
                $socialIconArray[$social_icon_link] = $request->social_icons[$key];
            }
            $data_array['social_icon'] = json_encode($socialIconArray);

            foreach ($request->address as $key => $addres) {
                $addressArray[$addres] = $request->address_titles[$key];
            }
            $data_array['address'] = json_encode($addressArray);


            if (isset($config)) {
                $config->update($data_array);
                $config->save();
            } else {
                Configration::create($data_array);
            }
            return redirect()->route('admin.web.general.setting')->with('success', 'Data updated!');
        }catch (Exception $ex)
        {
            return redirect()->route('admin.web.general.setting')->with('error-message', 'Something went wrong please try again!');
        }

    }


    function SMTPSetting()
    {
        $smtp = Configration::select('smtp', 'id')->first();
        if (isset($smtp)) {
            $smtp = json_decode($smtp->smtp);
        }
        return view('admin.webSetting.smtp', compact('smtp'));
    }

    function SaveSMTPSetting(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mail_mailer' => ['required'],
            'mail_host' => 'required',
            'mail_port' => 'required',
            'mail_username' => 'required',
            'mail_password' => 'required',
            'mail_encryption' => 'required',
            'mail_from_address' => 'required',
            'mail_from_name' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        try {
            $config = Configration::first();
            if (isset($config)) {
                $config->update([
                    'smtp' => json_encode($request->except('_token')),
                ]);
            } else {
                Configration::create([
                    'smtp' => json_encode($request->except('_token'))
                ]);
            }
            return redirect()->back()->with('success', 'Data updated!');
        } catch (Exception $ex) {
            return redirect()->back()->with('error-message', 'Something went wrong please try again!');
        }
    }

}
