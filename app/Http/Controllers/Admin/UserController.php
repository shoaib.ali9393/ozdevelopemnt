<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index()
    {
        $users = User::where('role', '!=', User::SUPERADMIN)
            ->where('id', "!=", Auth::user()->id)
            ->get();
        return view('admin.users.list', compact('users'));
    }

    public function show()
    {
        return view('admin.users.add-user');
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'role' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        try {
            User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'role' => $request->role,
            ]);
            return redirect()->back()->with('success', 'User added successfully!');
        } catch (Exception $ex) {
            return redirect()->back()->with('error-message', 'Something went wrong please try again!');
        }

    }

    function delete(Request $request)
    {
        try {
            User::where('id', $request->id)->delete();
        } catch (Exception $ex) {
            return redirect()->back()->with('error-message', 'Something went wrong please try again!');
        }
        return redirect()->back()->with('success', 'User deleted successfully!');
    }

    function edit(Request $request)
    {
        try {
            $user = User::where('id', $request->id)->first();
        } catch (Exception $ex) {
            return redirect()->back()->with('error-message', 'Something went wrong please try again!');
        }

        if (isset($user)) {
            return view('admin.users.edit', compact('user'));
        }
        return redirect()->back()->with('error', 'No user found!');
    }

    function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => 'required|email|unique:users,email,' . $request->id,
            'role' => 'required',
            'password' => 'sometimes|nullable|string|min:8|confirmed',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        try {
            $user = User::where('id', $request->id)->first();
            if (isset($user)) {
                $user->update([
                    'name' => $request->name,
                    'email' => $request->email,
                    'role' => $request->role,
                ]);
                if (isset($request->password)) {
                    $user->update([
                        'password' => Hash::make($request->password),
                    ]);
                }
                $user->save();
                return redirect()->back()->with('success', 'User updated successfully!');
            }
        } catch (Exception $ex) {
            return redirect()->back()->with('error-message', 'Something went wrong please try again!');
        }
    }
}

