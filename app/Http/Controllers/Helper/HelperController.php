<?php

namespace App\Http\Controllers\Helper;

use App\Http\Controllers\Controller;
use App\Models\Gallery;
use Exception;

class HelperController extends Controller
{
    public function ImageProcess($image, $name, $alt, $type, $id)
    {
        $data_array = [
            'type' => $type
        ];
        if ($name != null) {
            $data_array['name'] = $name;
        }
        if ($alt != null) {
            $data_array['alt'] = $alt;
        }
        if ($image != null) {
            $imageName = $name . '.' . $image->extension();
            $image->move(public_path('/upload/'), $imageName);
            $data_array['image'] = $imageName;
        }
        if ($id != null) {
            $this->unlinkImage($id);
            $galleryData = Gallery::where('id', $id)->first();
            $galleryData->update($data_array);
            $galleryData->refresh();
            return $galleryData;
        } else {
            $galleryData = Gallery::create($data_array);
            return $galleryData;
        }

    }

    public function unlinkImage($id)
    {
        $gallery = Gallery::where('id', $id)->first();
        $file_path = public_path() . '/upload/' . $gallery->image;
       if (file_exists($file_path)){
            unlink($file_path);
        }
    }
}
