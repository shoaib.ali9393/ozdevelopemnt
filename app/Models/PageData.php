<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PageData extends Model
{
    use HasFactory;
    protected $fillable=[
        'id',
        'page_id',
        'faq',
    ];

    function page()
    {
        return $this->belongsTo(Page::class , 'page_id' , 'id');
    }
}
