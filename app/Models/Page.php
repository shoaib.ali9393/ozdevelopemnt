<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasFactory;

    const HOME  = "Home";
    const BLOGS  = "Blogs";
    const CONTACTUS  = "Contact Us";
    const PRIVACY  = "Privacy";

    protected $fillable = [
        'id',
        'page',
        'content',
        'meta_title',
        'meta_description',
        'index_status',
        'status',
        'slug',
    ];
}
