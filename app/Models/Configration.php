<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Configration extends Model
{
    use HasFactory;
    protected $fillable=[
        'id',
        'footer_logo_id',
        'header_logo_id',
        'header_links',
        'footer_links',
        'email',
        'phone',
        'map',
        'copy_right',
        'working_hours',
        'social_icon',
        'address',
        'smtp',
    ];


    public function header_logo()
    {
        return $this->belongsTo(Gallery::class,'header_logo_id' , 'id');
    }
    public function footer_logo()
    {
        return $this->belongsTo(Gallery::class,'footer_logo_id' , 'id');
    }
}
