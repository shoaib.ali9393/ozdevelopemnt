<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    use HasFactory;
    const Image = "image";
    protected $fillable = [
        'id',
        'image',
        'name',
        'alt',
        'type',
    ];
}
