<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configrations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('header_logo_id')->nullable();
            $table->unsignedBigInteger('footer_logo_id')->nullable();
            $table->longText('header_links')->nullable();
            $table->longText('footer_links')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->longText('map')->nullable();
            $table->string('copy_right')->nullable();
            $table->string('working_hours')->nullable();
            $table->longText('social_icon')->nullable();
            $table->longText('address')->nullable();
            $table->longText('smtp')->nullable();
            $table->foreign('header_logo_id')->references('id')->on('galleries');
            $table->foreign('footer_logo_id')->references('id')->on('galleries');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configrations');
    }
};
