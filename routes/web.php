<?php

use App\Http\Controllers\Admin\FaqController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\HomePageController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\WebController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('web.index');
})->name('index');
Route::get('/about-us', function () {
    return view('web.about-us');
})->name('about-us');
Route::get('/contact-us', function () {
    return view('web.contact-us');
})->name('contact-us');
Route::get('/contact-us/form', function () {
    return view('web.contact-us-form');
})->name('contact-us-form');
Route::get('/app-development', function () {
    return view('web.services.app-development');
})->name('app-development');
Route::get('/web-development', function () {
    return view('web.services.web-development');
})->name('web-development');
Route::get('/web-design', function () {
    return view('web.services.web-design');
})->name('web-design');
Route::get('/pricing', function () {
    return view('web.pricing');
})->name('pricing');
Route::get('/blog', function () {
    return view('web.blog.blog');
})->name('blog');
Route::get('/blog-post', function () {
    return view('web.blog.blog-post');
})->name('blog-post');

Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//Admin Routes
Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function () {

    Route::get('/home', [HomeController::class, 'index'])->name('admin.home');


    Route::group(['prefix' => 'user', 'middleware' => []], function () {
        Route::get('/list', [UserController::class, 'index'])->name('admin.user.list');
        Route::get('/add', [UserController::class, 'show'])->name('admin.add.user.show');
        Route::post('/save', [UserController::class, 'save'])->name('admin.add.user.save');
        Route::get('/delete/{id}', [UserController::class, 'delete'])->name('admin.user.delete');
        Route::get('/edit/{id}', [UserController::class, 'edit'])->name('admin.user.edit');
        Route::post('/update', [UserController::class, 'update'])->name('admin.user.update');
    });
    Route::get('/smtp/setting', [WebController::class, 'SMTPSetting'])->name('admin.edit.smtp');
    Route::post('/save/smtp/setting', [WebController::class, 'SaveSMTPSetting'])->name('admin.edit.save.smtp');

    Route::group(['prefix' => 'web/general/setting', 'middleware' => []], function () {
        Route::get('/page', [WebController::class, 'index'])->name('admin.web.general.setting');
        Route::post('/save/general/setting', [WebController::class, 'saveGeneralSetting'])->name('admin.save.general.setting');
    });

    Route::group(['prefix' => 'home', 'middleware' => []], function () {
        Route::get('/page', [HomePageController::class, 'index'])->name('admin.home.page.index');
        Route::get('/add/page', [HomePageController::class, 'addHomepage'])->name('admin.add.home.page');
        Route::post('/save/page/data', [HomePageController::class, 'saveData'])->name('admin.save.page.data');
        Route::get('/edit/page', [HomePageController::class, 'editPage'])->name('admin.edit.home.page');
        Route::post('/update/page', [HomePageController::class, 'updatePage'])->name('admin.update.home.page');
    });

    Route::group(['prefix' => 'faq', 'middleware' => []], function () {
        Route::get('/index' , [FaqController::class , 'index'])->name('admin.faq.list');
        Route::get('/add' , [FaqController::class , 'addFaq'])->name('admin.faq.add');
        Route::post('/save' , [FaqController::class , 'save'])->name('admin.faq.save');
        Route::get('/edit/{id}' , [FaqController::class , 'editFaq'])->name('admin.faq.edit');

    });
});

